﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableInit : MonoBehaviour
{
    public Color myColor;
    // Start is called before the first frame update
    void Start()
    {
        Init();
    }
    public void Init( Color clr)
    {
        myColor = clr;
        gameObject.GetComponent<Renderer>().material.color = clr;
        
    }
    public void Init()
    {
        myColor = StackManager.singleton.availableColors[Random.Range(0, 3)];
        gameObject.GetComponent<Renderer>().material.color = myColor;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
