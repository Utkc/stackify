﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementManager : MonoBehaviour
{
    Transform self;
    public float playerSpeed;
    public float playerSideSpeed;
    // Start is called before the first frame update
    void Start()
    {
        self = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Vertical") != 0)
        {
            self.position = new Vector3(self.position.x, self.position.y, self.position.z + Input.GetAxis("Vertical") * playerSpeed);
        }
        if (Input.GetAxis("Horizontal") != 0)
        {
            self.position = new Vector3(self.position.x + Input.GetAxis("Horizontal") * playerSideSpeed, self.position.y , self.position.z );
        }
    }


}
