﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StackManager : MonoBehaviour
{
    public static StackManager singleton;

    Transform self;
    public Color MyColor;
    public List<Color> availableColors;

    public List<GameObject> collecteds;
    public float totalheight;
    public float gap;
    // Staris called before the first frame update
    void Awake()
    {
        if(singleton == null)
            singleton = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        gameObject.GetComponent<Renderer>().material.color = MyColor;
        self = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetMyColor(Color clr)
    {
        MyColor = clr;
        gameObject.GetComponent<Renderer>().material.color = MyColor;
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision Detected");
        Debug.Log("1 :" + other.gameObject.GetComponent<CollectableInit>().myColor);
        Debug.Log("2 :" + MyColor);
        Debug.Log("3 :" + other.gameObject.GetComponent<CollectableInit>().myColor.Equals(MyColor));

        if (other.gameObject.GetComponent<CollectableInit>().myColor.IsEqualTo(MyColor) )
        {
            addToStack(other);
        }
        else
        {
            removeFromStack(other);
        }
    }

    public void removeFromStack(Collider other)
    {

        Debug.Log("Removed");
        if(collecteds.Count > 0)
        {
            totalheight -= collecteds[collecteds.Count - 1].transform.localScale.y - gap ;
            GameObject.Destroy(collecteds[collecteds.Count-1]);
            collecteds.RemoveAt(collecteds.Count-1);
            
        }
        GameObject.Destroy(other.gameObject);
    }

    public void addToStack(Collider other)
    {
        Debug.Log("Added");
        collecteds.Add(other.gameObject);
        totalheight += other.gameObject.transform.localScale.y / 2 + gap/2;
        other.gameObject.transform.parent = self;
        other.gameObject.transform.localPosition = new Vector3(0, totalheight, 0);
        totalheight += other.gameObject.transform.localScale.y / 2 + gap / 2;
    }
}
static class Extension
{
    public static bool IsEqualTo(this Color me, Color other)
    {
        Debug.Log("r" + me.r + "g" + me.g + "b" + me.b);
        Debug.Log("r" + other.r + "g" + other.g + "b" + other.b);
        return me.GetHashCode() == other.GetHashCode();
        return me.r == other.r && me.g == other.g && me.b == other.b && me.a == other.a;
    }
}